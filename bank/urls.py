from django.urls import include, path

from bank.views import ListCurrency


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', ListCurrency.as_view()),   # Defined a path that maps to the ListCurrency view
]
