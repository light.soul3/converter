from rest_framework.views import APIView
from rest_framework.response import Response
import requests
from .serializers import ConverterSerializer

# Created a custom exception for handling currency errors
class CurrencyError(Exception):
    msg = "Currency not aviable"
    status = 400

    def __init__(self, *args: object, msg=None, status=None) -> None:
        super().__init__(*args)
        self.msg = msg if msg is not None else self.msg
        self.status = status if status is not None else self.status


class ListCurrency(APIView):
    @staticmethod
    def get_currency_dict():
        # Get currency data from the API
        r = requests.get(
            "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"
        )

        # Check if the request was successful
        if r.status_code != 200:
            raise CurrencyError()

        # Create a dictionary of currency data
        currency_dict = {}
        for item in r.json():
            cc = item["cc"]
            currency_dict[cc] = item

        # Added a default entry for UAH because it's not present in the API response
        currency_dict["UAH"] = {
            "r030": 0,
            "txt": "Українська гривня",
            "rate": 1,
            "cc": "UAH",
            "exchangedate": "19.12.2023",
        }
        # Return Dictionary
        return currency_dict


    def post(self, request, *args, **kwargs):
        try:
            # Get dictionary od currencies
            currencies = self.get_currency_dict()
            
            # Validate data using the ConverterSerializer
            serializer = ConverterSerializer(data=request.data)

            if serializer.is_valid():

                # Extract validated data from the serializer
                curr = serializer.validated_data["current_currency"]
                conv = serializer.validated_data["convert_currency"]
                val = serializer.validated_data["value"]
                
                # Calculate the converted value and exchange rate
                curr_rate = currencies[curr]["rate"]
                conv_rate = currencies[conv]["rate"]
                value = (val*curr_rate)/conv_rate
                rate = curr_rate/conv_rate

                # Craete the output dictionary and provide it a value
                output = {"currency": conv, "value":value, "rate":rate}

                # Return the output as a JSON response
                return Response(output)
            else:
                # Return validation errors as a JSON response with a 400 status code
                return Response(serializer.errors, status=400)
        except CurrencyError as e:
            # Handle CurrencyError exceptions by returning an error message and status code
            return Response({"error": e.msg}, status=e.status)
        
