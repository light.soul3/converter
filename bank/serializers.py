from rest_framework import serializers

# Creted a list of currency codes
CURRENCY_CODES = ['UAH', 'AUD', 'CAD', 'CNY', 'CZK', 'DKK', 'HKD', 'HUF', 'INR', 'IDR', 'ILS', 'JPY', 'KZT', 'KRW', 'MXN', 'MDL', 'NZD', 'NOK', 'RUB', 'SGD', 'ZAR', 'SEK', 'CHF', 'EGP', 'GBP', 'USD', 'BYN', 'AZN', 'RON', 'TRY', 'XDR', 'BGN', 'EUR', 'PLN', 'DZD', 'BDT', 'AMD', 'DOP', 'IRR', 'IQD', 'KGS', 'LBP', 'LYD', 'MYR', 'MAD', 'PKR', 'SAR', 'VND', 'THB', 'AED', 'TND', 'UZS', 'TWD', 'TMT', 'RSD', 'TJS', 'GEL', 'BRL', 'XAU', 'XAG', 'XPT', 'XPD']
# Convert the list of currency codes into choices for the ChoiceField
CURRENCY_CODES_CHOICES = list((code, code) for code in CURRENCY_CODES)

#Created ConverterSerializer
class ConverterSerializer(serializers.Serializer):
    #Serializer feilds
    current_currency = serializers.ChoiceField(choices=CURRENCY_CODES_CHOICES)
    convert_currency = serializers.ChoiceField(choices=CURRENCY_CODES_CHOICES)
    value = serializers.FloatField()

    #Methods with are not used, but they are required for serializer
    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass
